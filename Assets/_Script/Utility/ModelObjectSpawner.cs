﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelObjectSpawner : MonoBehaviour
{
    public Transform spawnPos;

    private GameObject spawnedObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackToSelection()
    {
        Destroy(spawnedObject);
    }

    public void SpawnObject(GameObject modelToSpawn)
    {
        spawnedObject = Instantiate(modelToSpawn, Vector3.zero, Quaternion.identity);
    }

    public void SpawnObjectToSpawnPos(GameObject modelToSpawn)
    {
        spawnedObject = Instantiate(modelToSpawn, spawnPos.position, Quaternion.identity);
    }

}
