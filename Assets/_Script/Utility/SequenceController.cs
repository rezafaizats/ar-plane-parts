﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[Serializable]
public enum SequenceEnum
{
    NORMAL,
    CAUTION,
    ANIMATION,
    NOTE
}

[Serializable]
public class SequencesData
{
    public string sequenceTitle;
    public SequenceEnum sequenceType;
    [TextArea(5, 20)] public string sequenceDetails;
    public GameObject sequenceAnimation;
}

public class SequenceController : MonoBehaviour
{
    public SequencesData[] sequencesDatas;
    public GameObject sequenceTextNormal;
    public GameObject sequenceTextCaution;
    public GameObject sequenceTextAnimation;
    public GameObject sequenceTextNote;
    public GameObject contentContainerParent;
    public GameObject objectSpawnParent;
    private int sequenceLength;
    private int sequenceNow;

    public GameObject nextButton;
    public GameObject prevButton;

    // Start is called before the first frame update
    void Start()
    {
        sequenceLength = sequencesDatas.Length;
        sequenceNow = 0;

        if (sequencesDatas == null)
            return;

        if (sequenceNow <= 0)
            prevButton.SetActive(false);

        SpawnSequenceData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ClearAllContent()
    {
        for (int i = 0; i < objectSpawnParent.transform.childCount; i++)
        {
            Destroy(objectSpawnParent.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < contentContainerParent.transform.childCount; i++)
        {
            Destroy(contentContainerParent.transform.GetChild(i).gameObject);
        }

    }

    public void ShowNextSequence()
    {
        nextButton.SetActive(true);
        prevButton.SetActive(true);

        ClearAllContent();
        sequenceNow++;

        if (sequenceNow >= sequenceLength - 1) {
            nextButton.SetActive(false);
            sequenceNow = sequenceLength - 1;
        }

        SpawnSequenceData();
    }

    public void ShowPrevSequence()
    {
        nextButton.SetActive(true);
        prevButton.SetActive(true);

        ClearAllContent();
        sequenceNow--;

        if (sequenceNow <= 0)
        {
            prevButton.SetActive(false);
            sequenceNow = 0;
        }

        SpawnSequenceData();

    }

    void SpawnSequenceData()
    {

        switch (sequencesDatas[sequenceNow].sequenceType)
        {
            case SequenceEnum.NORMAL:
                TextMeshProUGUI normalText = Instantiate(sequenceTextNormal, contentContainerParent.transform)
                    .GetComponentInChildren<TextMeshProUGUI>();
                normalText.text = sequencesDatas[sequenceNow].sequenceDetails;
                break;
            case SequenceEnum.CAUTION:
                TextMeshProUGUI cautionText = Instantiate(sequenceTextCaution, contentContainerParent.transform)
                    .GetComponentInChildren<TextMeshProUGUI>();
                cautionText.text = sequencesDatas[sequenceNow].sequenceDetails;
                break;
            case SequenceEnum.ANIMATION:
                TextMeshProUGUI animationText = Instantiate(sequenceTextAnimation, contentContainerParent.transform)
                    .GetComponentInChildren<TextMeshProUGUI>();
                animationText.text = sequencesDatas[sequenceNow].sequenceDetails;
                Instantiate(sequencesDatas[sequenceNow].sequenceAnimation, objectSpawnParent.transform);
                break;
            case SequenceEnum.NOTE:
                TextMeshProUGUI noteText = Instantiate(sequenceTextNote, contentContainerParent.transform)
                    .GetComponentInChildren<TextMeshProUGUI>();
                noteText.text = sequencesDatas[sequenceNow].sequenceDetails;
                break;
            default:
                break;
        }

    }

}
