﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ImageUICanvasObjectScaler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool _isDragging;
    public float minHeight, minWidth;
    public float maxHeight, maxWidth;
    private float _temp = 0;
    public float _scalingRate = 250;
    private RectTransform _rectTransform;
    private Vector2 _currentSizeDelta;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _currentSizeDelta = _rectTransform.sizeDelta;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _isDragging = true;
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        _isDragging = false;
    }


    private void Update()
    {
        if (Input.touchCount == 2)
            {
            //transform.localScale = new Vector2(_currentScale, _currentScale);
            _rectTransform.sizeDelta = _currentSizeDelta;
            float distance = Vector3.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            if (_temp > distance)
                {
                if (_currentSizeDelta.x < minWidth || _currentSizeDelta.y < minHeight) {
                    print("minimal size");
                    return;
                }
                    //_currentScale -= (Time.deltaTime) * _scalingRate;
                _currentSizeDelta.x -= (Time.deltaTime) * _scalingRate;
                _currentSizeDelta.y -= (Time.deltaTime) * _scalingRate;
            }

            else if (_temp < distance)
                {
                if (_currentSizeDelta.x > maxWidth || _currentSizeDelta.y > maxHeight) {
                    print("max size");
                    return;
                }
                    //_currentScale += (Time.deltaTime) * _scalingRate;
                _currentSizeDelta.x += (Time.deltaTime) * _scalingRate;
                _currentSizeDelta.y += (Time.deltaTime) * _scalingRate;
            }

            _temp = distance;
        }
    }

    public void ScaleUp()
    {
        _currentSizeDelta.x += _scalingRate;
        _currentSizeDelta.y += _scalingRate;
        _rectTransform.sizeDelta = _currentSizeDelta;
    }

    public void ScaleDown()
    {
        _currentSizeDelta.x -= _scalingRate;
        _currentSizeDelta.y -= _scalingRate;
        _rectTransform.sizeDelta = _currentSizeDelta;
    }

}
